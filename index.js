class PluginTestCompletion extends StudIO.autocomplete {
    getSmartKeys() {
		return ["{", "}", this.suggestion, this.suggestion, this.suggestion, "'", "(", ")"]
	}
}
StudIO.add(PluginTestCompletion, "hint")